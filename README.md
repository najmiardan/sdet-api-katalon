Comparator API Test Automation using Katalon

1. compare function able to compare two JSON responses. [using katalon built-in library]

2. code should be capable of comparing millions of API requests without any memory issues. [Mandatory: Test with at least 1000 requests]

3. Print the output when responses are compared in the below format.

	https://reqres.in/api/users/3 not equals https://reqres.in/api/users/1
	https://reqres.in/api/users/2 equals https://reqres.in/api/users/2
	https://reqres.in/api/users?page=1 equals https://reqres.in/api/users?page= 1

4. Using built-in data-driven from katalon but limited to single test data as this is using unlicensed (free) katalon
*more than 1 test data for single test suite only available for katalon enterprise license

5. Using test suite to run test with multiple test data
